﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OefeningenReeks8.Entities;
using System.Collections;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace OefeningenReeks8.Controllers
{
    public class EmployeeController : Controller
    {
        private AdventureWorks2012Context db = null;

        public EmployeeController(AdventureWorks2012Context ctx)
        {
            this.db = ctx;
        }

        public IActionResult Index()
        {
            return View(db.Employee.Select(m => m).ToList());
        }
    }
}
